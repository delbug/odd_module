// efd.cpp: 定义 DLL 应用程序的导出函数。
//
#include "windows.h"
#include "winbase.h"
#include "stdafx.h"
#include "efd.h"
#include "malloc.h"
#include <tchar.h> 
#include <time.h> 
#include <stdlib.h>
#include "malloc.h"
#include "string.h"


using namespace Microsoft::Win32;
using namespace Fiddler;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Text;
using namespace System::IO;
using namespace System::Threading;
using namespace System::Runtime::InteropServices;



CallbackFun Callback = NULL;
void Copy_string(char* str1, char* str2);
bool autoProxy = false;
bool startSSL = false;
char *base64;
const char *delim = "|";//分隔符

//外部接口 - 关闭Fiddler
void _stdcall CloseFiddler()
{
	FiddlerApplication::Shutdown();
}





//外部接口 - 初始化Fiddler   void _stdcall StartSSL(bool start)
 int _stdcall InitFiddler(int iPort,int callback)
{
	 
	 //srand ((unsigned int)time(NULL));;
	 //Fiddler::CertMaker
	 Proxy ^oSecureEndpoint;
	 void Send_os(Session ^os);
	 void Recv_os(Session ^os);
	 void onwebsocket(Object ^sender, WebSocketMessageEventArgs ^os);
	 Callback = (CallbackFun)callback;
	 int iSecureEndpointPort = 7777;
	 String ^sSecureEndpointHostname = "localhost";
	 CONFIG::IgnoreServerCertErrors = true;
	 FiddlerApplication::Prefs->SetBoolPref("fiddler.network.streaming.abortifclientaborts", true);
	 FiddlerCoreStartupSettings ^startupSettings;
	 FiddlerCoreStartupSettingsBuilder ^c = gcnew FiddlerCoreStartupSettingsBuilder();

	 c = c->ListenOnPort((USHORT)iPort);
	 c = c->AllowRemoteClients();
	 c = c->ChainToUpstreamGateway();
	 c = c->MonitorAllConnections();
	 c = c->HookUsingPACFile();
	 c = c->CaptureLocalhostTraffic();
	 c = c->OptimizeThreadPool();
	 
	 if (autoProxy)
	 {
		 c = c->RegisterAsSystemProxy();
	 }
	 if (startSSL)
	 {
		 c = c->DecryptSSL();
	 }

	 startupSettings = c->Build();
	 
	 FiddlerApplication::Startup(startupSettings);
	 oSecureEndpoint = FiddlerApplication::CreateProxyEndpoint(iSecureEndpointPort, true, sSecureEndpointHostname);
	 
	 Fiddler::FiddlerApplication::BeforeRequest += gcnew SessionStateHandler(Send_os);//SessionStateHandler
	 Fiddler::FiddlerApplication::BeforeResponse += gcnew SessionStateHandler(Recv_os);
	 Fiddler::FiddlerApplication::OnWebSocketMessage += gcnew EventHandler<WebSocketMessageEventArgs^>(onwebsocket);//EventHandler<WebSocketMessageEventArgs^>^ value
	 return 1;
}

 //外部接口 - 删除证书
 bool _stdcall removeCerts()
 {
	 if (CertMaker::removeFiddlerGeneratedCerts()) {
		 return 1;
	 }
	 return -1;

 }
 //外部接口 - 安装证书
 int _stdcall InstCert()
 {
		 //TrustRootCertificate();
	 //添加证书到信任根
	 if (!CertMaker::trustRootCert())
	 {		
		 return -1;
	 }
	 return 1;
 }

 //外部接口 - 创建root根证书 （需要依赖C***相关dll文件，易语言运行模式下记得copy到e_debug）
 int _stdcall CreateRootCert()
 {
	 
	 if (!CertMaker::createRootCert()) {
		 return 0;
	 }

	 
	 return 1;
 }

 //外部接口 - 检查根证书是否存在
 bool _stdcall rootCertExists()
 {
	 return CertMaker::rootCertExists();

 }

 //检查是否安装机器证书
 bool _stdcall rootCertIsMachineTrusted()
 {
	 return CertMaker::rootCertExists();
 }

 //检查是否安装root证书
  bool _stdcall rootCertIsTrusted()
 {
	  return CertMaker::rootCertIsTrusted();
 }

 //外部接口 - 设置自动运行代理
 void _stdcall AutoStartFiddlerProxy(bool start)
 {
	 autoProxy = start;
 }

 //外部接口 - 设置是否开启SSL抓包
 void _stdcall StartSSL(bool start)
 {
	 startSSL = start;
 }

 void _stdcall SetBase64(int* url)
 {
	 base64 = (char *)malloc(1024);
	 Copy_string((char *)url,base64);

 }


 //监听到websocket数据
 void onwebsocket(Object ^sender, WebSocketMessageEventArgs ^os)
 {
	 void pw_data(WebSocketMessage ^ms, int *t);
	 WebSocketMessage ^message = os->oWSM;

	 int *p, *t;
	 
	 p = (int *)malloc(sizeof(int) * 9 + 1);
	 t = (int *)malloc(sizeof(int) * 30 + 1);
	 memset(p, 0, sizeof(int) * 9 + 1);
	 memset(t, 0, sizeof(int) * 30 + 1);
	
	 char* url = (char*)(void*)Marshal::StringToHGlobalAnsi(message->FrameType.ToString());
	 char* head = (char*)(void*)Marshal::StringToHGlobalAnsi(message->iCloseReason.ToString());
	 char* cookie = (char*)(void*)Marshal::StringToHGlobalAnsi(message->ID.ToString());
	 char* LocalProcess = (char*)(void*)Marshal::StringToHGlobalAnsi(message->IsFinalFrame.ToString());
	 char* postdata = (char*)(void*)Marshal::StringToHGlobalAnsi(message->PayloadAsString());
	 char* contenttype = (char*)(void*)Marshal::StringToHGlobalAnsi(message->IsOutbound.ToString());
	 char* size = (char*)(void*)Marshal::StringToHGlobalAnsi(message->PayloadLength.ToString());
	 *(p) = (int)url;//指针地址1（1，5）
	 *(p + 1) = (int)head;//指针地址2（5，9）
	 *(p + 2) = (int)cookie;//指针地址3（9，13）
	 *(p + 3) = (int)LocalProcess;//指针地址4（13，17）
	 *(p + 4) = (int)postdata;//指针地址5（17，21）
	 *(p + 5) = (int)contenttype;//指针地址5（21，25）
	 *(p + 6) = (int)size;//指针地址5（25，29）
	 *(p + 7) = (int)message->ID;
	 *(p + 8) = 2;//指针地址6（29,33）
	 Callback((int)p, (int)t);//(int)*p
	 pw_data(message, t);
	 postdata = nullptr;
	 cookie = nullptr;
	 head = nullptr;
	 url = nullptr;
	 LocalProcess = nullptr;
	 free(p);

 }

 void Send_os(Session ^os)
 {
	 if (strstr((char*)(void*)Marshal::StringToHGlobalAnsi(os->fullUrl), "443")>0)
	 {
		 return;
	 }
	 void pm_data(Session ^os, int *t);
	 int *p,*t;
	 p = (int *)malloc(sizeof(int) * 9+1);
	 t = (int *)malloc(sizeof(int) * 30+1);
	 memset(p, 0, sizeof(int) * 9+1);
	 memset(t, 0, sizeof(int) * 30+1);
	 Fiddler::ClientChatter ^request = os->oRequest;
	 char* url = (char*)(void*)Marshal::StringToHGlobalAnsi(os->fullUrl);
	 char* head = (char*)(void*)Marshal::StringToHGlobalAnsi(os->oRequest->headers->ToString());
	 char* cookie = (char*)(void*)Marshal::StringToHGlobalAnsi(request["Cookie"]);
	 char* LocalProcess = (char*)(void*)Marshal::StringToHGlobalAnsi(os->LocalProcess);
	 char *str;
	 char *next_token = NULL;
	 char *postdata = NULL;
	 bool isbase = false;

	 if (base64 != NULL) {
		 if (strstr(url, base64) > 0)
		 {
			 isbase = true;
		 }

	 }
	if(isbase)
	 {
		 postdata = (char*)(void*)Marshal::StringToHGlobalAnsi(Convert::ToBase64String(os->requestBodyBytes));
	 }
	 else
	 {
		 postdata = (char*)(void*)Marshal::StringToHGlobalAnsi(os->GetRequestBodyAsString());
	 }

	 char* contenttype = (char*)(void*)Marshal::StringToHGlobalAnsi(os->oResponse["Content-Type"]);
	 char* size = (char*)(void*)Marshal::StringToHGlobalAnsi(os->requestBodyBytes->Length.ToString());
	 *(p ) = (int)url;//指针地址1（1，5）
	 *(p + 1) = (int)head;//指针地址2（5，9）
	 *(p + 2) = (int)cookie;//指针地址3（9，13）
	 *(p + 3) = (int)LocalProcess;//指针地址4（13，17）
	 *(p + 4) = (int)postdata;//指针地址5（17，21）
	 *(p + 5) = (int)contenttype;//指针地址5（21，25）
	 *(p + 6) = (int)size;//指针地址5（25，29）
	 *(p + 7) = (int)os->id;
	 *(p + 8) = 1;//指针地址6（29,33）
	 Callback((int)p,(int)t);//(int)*p
	 pm_data(os, t);
	 postdata = nullptr;
	 cookie = nullptr;
	 head = nullptr;
	 url = nullptr;
	 LocalProcess = nullptr;
	 free(p);
 }

 void Recv_os(Session ^os)
 {
	 if (strstr((char*)(void*)Marshal::StringToHGlobalAnsi(os->fullUrl), "443") > 0)
	 {
		 return;
	 }
	 void pm_data(Session ^os, int *t);
	 int *p,*t;
	 p = (int *)malloc(sizeof(int) * 9+1);
	 t = (int *)malloc(sizeof(int) * 30+1);
	 memset(p, 0, sizeof(int) * 9+1);
	 memset(t, 0, sizeof(int) * 30 +1);



	 Fiddler::ServerChatter ^response = os->oResponse;
	 Fiddler::ClientChatter ^request = os->oRequest;
	 char* url = (char*)(void*)Marshal::StringToHGlobalAnsi(os->fullUrl);
	 char* head = (char*)(void*)Marshal::StringToHGlobalAnsi(os->oResponse->headers->ToString());
	 char* cookie = (char*)(void*)Marshal::StringToHGlobalAnsi(request["Cookie"]);
	 char* LocalProcess = (char*)(void*)Marshal::StringToHGlobalAnsi(os->LocalProcess);
	 char* html = NULL;
	 char *str;
	 char *next_token = NULL;
	 bool isbase = false;
	 if (base64 != NULL) {
		 if (strstr(url, base64) > 0)
		 {
			 isbase = true;
		 }

	 }
	 if (isbase)
	 {
		html= (char*)(void*)Marshal::StringToHGlobalAnsi(Convert::ToBase64String(os->responseBodyBytes));
	 }
	 else
	 {
		 html = (char*)(void*)Marshal::StringToHGlobalAnsi(os->GetResponseBodyAsString());
	 }
	 
	 char* contenttype = (char*)(void*)Marshal::StringToHGlobalAnsi(os->oResponse["Content-Type"]);
	 char* size = (char*)(void*)Marshal::StringToHGlobalAnsi(os->responseBodyBytes->Length.ToString());

	 *p = (int)url;//指针地址1（1，5）
	 *(p + 1) = (int)head;//指针地址2（5，9）*/
	 *(p + 2) = (int)cookie;//指针地址3（9，13）
	 *(p + 3) = (int)LocalProcess;//指针地址5（13，17）
	 *(p + 4) = (int)html;//指针地址6（17，21）
	 *(p + 5) = (int)contenttype;//指针地址6（21，25）
	 *(p + 6) = (int)size;//指针地址6（25，29）
	 *(p + 7) = (int)os->id;
	 *(p + 8) = 0;//指针地址7（29，33）*/
	 
	 Callback((int)p,(int)t);//(int)*p
	 pm_data(os, t);
	 html = nullptr;
	 cookie = nullptr;
	 head = nullptr;
	 url = nullptr;
	 LocalProcess = nullptr;
	 free(p);
	 free(t);
 }

 void pm_data(Session ^os, int *t)
 {
	 os->bBufferResponse = true;
	 for (int i = 0; i < 10; i++)
	 {
		 int cfo = i * 3;//控制位  = 0
		 char cft = *(t + cfo);//控制符 = 0
		 if (cft == 0)
		 {
			 break;
		 }
		 
		 char* data1 = (char *)*(t + cfo + 1);//内存地址 = 5
		 char* data2 = (char *)*(t + cfo + 2);//内存地址 = 9
		 String^ str2 = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((IntPtr)data2);
		 String^ str1 = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((IntPtr)data1);
		 switch (cft)
		 {
			 case 1://控制符 = 1 代表替换网页源码字符串 (recv)√ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->utilReplaceInResponse(str1, str2);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 2://控制符 = 2 代表重写整个返回的源码 （recv）√ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->utilSetResponseBody(str1);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 3://控制符 = 3 在顶部增加新data （recv）√ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->utilPrependToResponseBody(str1);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 4://控制符 = 4 设置新的set-cookie （recv）
			 {
				 os->utilDecodeResponse();
				 os->oResponse->headers->Add("Set-Cookie", str1);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 5: //控制符 = 5 删除返回的set-cookie （recv）√ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->oResponse->headers->Remove("Set-Cookie");
				 os->utilDecodeRequest();
				 break;
			 }
			 case 6: //控制符 = 6 设置协议头 （recv）
			 {
				 os->utilDecodeResponse();
				 os->oResponse->headers->Remove(str1);
				 os->oResponse->headers->Add(str1, str2);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 7://控制符 = 7 设置新的网页源码_byte （recv）×直接崩溃，代码异常
			 {
				 os->utilDecodeResponse();
				 array<unsigned char>^ byte1;
				 byte1 = Convert::FromBase64String(str1);
				 os->ResponseBody = byte1;
				 os->utilDecodeRequest();

				 break;
			 }
			 // ========== send ========
			 case 8: //控制符 = 8 设置新的POST参数（send） √ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->utilSetRequestBody(str1);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 9: //控制符 = 9 设置新的POST参数_byte（send） √ 测试有效
			 {
 
				 os->utilDecodeResponse();
				 array<unsigned char>^ byte1;
				 byte1 = Convert::FromBase64String(str1);
				 os->RequestBody = byte1;
				 os->utilDecodeRequest();
				 
				 break;
				 
			 }
			 case 10: //控制符 = 10 设置新的URL地址（send）√ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->fullUrl = str1;
				 os->utilDecodeRequest();
				 break;

			 }
			 case 11: //控制符 = 11 替换URL地址（send）	√ 测试有效 (对部分URL直接未响应）
			 {
				 os->utilDecodeResponse();
				 os->fullUrl = os->fullUrl->Replace(str1, str2);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 12: //控制符 = 12 追加URL地址（send）	× 测试无效，未论证
			 {
				 os->utilDecodeResponse();
				 if (os->fullUrl->IndexOf('?')>0)
				 {
					 os->fullUrl = os->fullUrl + "&" + str1;
				 }
				 else
				 {
					 os->fullUrl = os->fullUrl + "?" + str1;
				 }
				 os->utilDecodeRequest();
				 break;
			 }
			 case 13: //控制符 = 13 替换POST数据（send）   √ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->utilReplaceInRequest(str1, str2);
				 os->utilDecodeRequest();
				 break;
			 }
			 case 14://控制符 = 14 直接设置cookie（send）	 √ 测试有效
			 {
				 os->utilDecodeResponse();
				 os->oRequest["Cookie"] = str1;
				 os->utilDecodeRequest();
				 break;
			 }
			 case 15://控制符 = 15 设定发送的协议头（send）	 
			 {
				 os->utilDecodeResponse();
				 os->oRequest->headers->Remove(str1);
				 os->oRequest->headers->Add(str1, str2);
				 os->utilDecodeRequest();
				 break;
			 }
		
		 }
		 
		 

	 }
	 
 }


 void pw_data(WebSocketMessage ^ms, int *t)
 {
	 for (int i = 0; i < 10; i++)
	 {
		 int cfo = i * 3;//控制位  = 0
		 char cft = *(t + cfo);//控制符 = 0
		 if (cft == 0)
		 {
			 break;
		 }

		 char* data1 = (char *)*(t + cfo + 1);//内存地址 = 5
		 char* data2 = (char *)*(t + cfo + 2);//内存地址 = 9
		 String^ str2 = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((IntPtr)data2);
		 String^ str1 = System::Runtime::InteropServices::Marshal::PtrToStringAnsi((IntPtr)data1);
		 switch (cft)
		 {
		 case 15://控制符 = 1 代表替换字符串（websocket）
		 {
			 ms->SetPayload(ms->PayloadAsString()->Replace(str1, str2));
			 break;
		 }
		 case 16://控制符 = 1 代表替换字符串（websocket）
		 {
			 ms->SetPayload(str1);
			 break;
		 }
		
		 }



	 }

 }




 void Copy_string(char* str1, char* str2)     //自定义字符串连接函数
 {

	 int i = 0;
	 while (str1[i] != 0)
	 {
		 str2[i] = str1[i];
		 i++;
	 }
	 str2[i] = '\0'; 
 }

